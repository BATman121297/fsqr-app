import axios from 'axios';

export default {

    async FETCH_TRANSACTION({commit}, { user_actions, token }){
      await axios({
        method: 'GET',
          url: `${this.$axios.defaults.baseURL}`,
          headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
          user_actions
        }
      }).then(res => {
        if (Array.isArray(res.data.delivery))
          commit('SET_TRANSCATION', res.data.delivery);

          const deliveryArray = [];
          res.data.delivery.forEach(element => {
              const data = {
                id: element.id,
                
              }

              deliveryArray.push(data);
          });

          // commit('setTransmitMainTableData', deliveryArray);
        }
      ).catch(err => console.log(err.response))
    },

    // async fetchTransferModalData({commit}, { user_actions, token }){
    //   await axios({
    //     method: 'GET',
    //       url: `${this.$axios.defaults.baseURL}/check/get/accounts-payable`,
    //       headers: {
    //         Authorization: `Bearer ${token}`
    //     },
    //     data: {
    //       user_actions
    //     }
    //   }).then(res => {
    //     if (Array.isArray(res.data.fetched))
    //       commit('setTransferModalData', res.data.fetched)
    //     }
    //   ).catch(err => console.log(err.response))
    // },

    // async fetchVoidData({commit}, { user_actions, token }){
    //   await axios({
    //     method: 'GET',
    //       url: `${this.$axios.defaults.baseURL}/check/void`,
    //       headers: {
    //         Authorization: `Bearer ${token}`
    //     },
    //     data: {
    //       user_actions
    //     }
    //   }).then(res => {
    //     if (Array.isArray(res.data.fetched))
    //       commit('setVoidData', res.data.fetched)
    //     }
    //   ).catch(err => console.log(err.response))
    // },

    // // Create Transaction (Cheque Writer)
    // async createTransaction({ commit }, {user_id, user_actions, token, transactionFields}) {
    //   console.log(transactionFields)
    //   await axios({
    //       method: 'POST',
    //       url: `${this.$axios.defaults.baseURL}/check/transfer`,
    //       headers: {
    //           Authorization: `Bearer ${token}`
    //       },
    //       data: {
    //         user_id,
    //         user_actions,
    //         ...transactionFields
    //       }
    //   }).then(res => {
    //     // console.log(res)
    //       commit('addCreatedTransaction', res.data)
    //   }).catch(err => console.log(err.response));
    // },

    // // Receive Transaction (Transmitter Group)

    // async fetchTransmitData({commit}, { user_actions, token }){
    //   const transmitTableArr = [];

    //   await axios({
    //     method: 'GET',
    //       url: `${this.$axios.defaults.baseURL}/transaction/transferred`,
    //       headers: {
    //         Authorization: `Bearer ${token}`
    //     },
    //     data: {
    //       user_actions
    //     }
    //   }).then(res => {
    //     if (Array.isArray(res.data.fetched))
    //       commit('setTransferData', res.data.fetched);

    //       res.data.fetched.forEach(element => {
    //           const data = {
    //             id: element.id,
    //             transactionNumber: element.transferNumber,
    //             description: element.description,
    //             controlCount: element.controlCount,
    //             controlAmount: element.controlAmount,
    //             isPosted: element.isPosted,
    //             transactionDate: element.transferDate,
    //             checkStatus: 'transferred'
    //           }

    //           transmitTableArr.push(data);
    //       });

    //     }
    //   ).catch(err => console.log(err.response))

    //   await axios({
    //     method: 'GET',
    //       url: `${this.$axios.defaults.baseURL}/transaction/transmit`,
    //       headers: {
    //         Authorization: `Bearer ${token}`
    //     },
    //     data: {
    //       user_actions
    //     }
    //   }).then(res => {
    //       if (Array.isArray(res.data.fetched)){
    //         // commit('setTransferData', res.data.fetched);
    //         res.data.fetched.forEach(element => {
    //             const data = {
    //               id: element.id,
    //               transactionNumber: element.transmittalNumber,
    //               description: element.description,
    //               controlCount: element.controlCount,
    //               controlAmount: element.controlAmount,
    //               isPosted: element.isPosted,
    //               transactionDate: element.transmittalDate,
    //               checkStatus: 'transmitted'
    //             }

    //             transmitTableArr.push(data);
    //         });
    //       };
    //     }).catch(err => console.log(err.response));

        
    //     commit('setTransmitMainTableData', transmitTableArr);
    // },

}