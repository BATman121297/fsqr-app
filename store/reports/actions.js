import axios from 'axios';

export default {

  async FETCH_RMAF_REPORTS({ commit }, { dateFrom, dateTo }) {

    const employee = JSON.parse(localStorage.employee);
    this.role = employee.roleParam;
    await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}`,
      data: {
        employee_id: employee.id,
        modules: employee.modules,
        request_date_from: dateFrom,
        request_date_to: dateTo
      },
      headers: {
        authorization: `B1SESSION=${employee.token}`
      }
    })
      .then(res => {
        commit('SET_RMAF_REPORTS', res.data);
      })
      .catch(e => {});
  },


  async FETCH_MRR_REPORTS({ commit }, { dateFrom, dateTo }) {

    const employee = JSON.parse(localStorage.employee);
    this.role = employee.roleParam;
    await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}`,
      data: {
        employee_id: employee.id,
        modules: employee.modules,
        request_date_from: dateFrom,
        request_date_to: dateTo
      },
      headers: {
        authorization: `B1SESSION=${employee.token}`
      }
    })
      .then(res => {
        commit('SET_MRR_REPORTS', res.data);
      })
      .catch(e => {});
  },

  async FETCH_REJECTION_REPORTS({ commit }, { dateFrom, dateTo }) {

    const employee = JSON.parse(localStorage.employee);
    this.role = employee.roleParam;
    await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}`,
      data: {
        employee_id: employee.id,
        modules: employee.modules,
        request_date_from: dateFrom,
        request_date_to: dateTo
      },
      headers: {
        authorization: `B1SESSION=${employee.token}`
      }
    })
      .then(res => {
        commit('SET_REJECTION_REPORTS', res.data);
      })
      .catch(e => {});
  },

  async FETCH_DEDUCTION_REPORTS({ commit }, { dateFrom, dateTo }) {

    const employee = JSON.parse(localStorage.employee);
    this.role = employee.roleParam;
    await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}`,
      data: {
        employee_id: employee.id,
        modules: employee.modules,
        request_date_from: dateFrom,
        request_date_to: dateTo
      },
      headers: {
        authorization: `B1SESSION=${employee.token}`
      }
    })
      .then(res => {
        commit('SET_DEDUCTION_REPORTS', res.data);
      })
      .catch(e => {});
  }

    

}