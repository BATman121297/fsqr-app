import Vue from 'vue';
import VueQrcode from '@chenfengyuan/vue-qrcode';
import VueCryptojs from 'vue-cryptojs'
import vueSignature from "vue-signature"

Vue.use(VueCryptojs);
Vue.use(vueSignature);
Vue.component(VueQrcode.name, VueQrcode);