export default function ({ redirect }) {
    let employee;
    if (localStorage.employee) {
        employee = JSON.parse(localStorage.employee);
        const mapScan = employee.modules.find(element =>
            element.actions.find(
                gatepass => gatepass.description === "View scan"
            )
        );
        if (localStorage.token && !mapScan ) {
            redirect('/sampling');
        }
    } else {
        redirect('/');
    }

    
    
}