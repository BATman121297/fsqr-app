export default function ({ redirect }) {
    let employee;
    if (localStorage.employee) {

        employee = JSON.parse(localStorage.employee)

        const mapAdmin = employee.modules.find(
            element => element.description === "admin"
        );
        
        if (localStorage.token && !mapAdmin) {
            redirect('/admin/modules')
        }
    } else {
        redirect('/')
    }
}