export default function ({ redirect }) {
    let employee;
    if (localStorage.employee) {
        employee = JSON.parse(localStorage.employee);
        const mapRMScan = employee.modules.find(element =>
            element.actions.find(
                gatepass => gatepass.description === "View rm scan"
            )
        );
        if (localStorage.token && !mapRMScan ) {
            redirect('/tally');
        }
    } else {
        redirect('/');
    }

    
    
}