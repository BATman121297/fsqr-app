export default function ({ redirect }) {

    let employee;
  
    if (localStorage.employee) {
  
      employee = JSON.parse(localStorage.employee);
      const sec = employee.roleParam === 'sec';
  
      if (localStorage.token && !sec) {
        redirect('/guard')
      }
    } else {
      redirect('/');
    }
  }
  