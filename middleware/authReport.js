export default function ({ redirect }) {
    let employee;
    if (localStorage.employee) {

        employee = JSON.parse(localStorage.employee)

        const mapAdmin = employee.modules.find(
            element => element.description === "reports"
        );
        
        if (localStorage.token && !mapAdmin) {
            redirect('/report')
        }
    } else {
        redirect('/')
    }
}